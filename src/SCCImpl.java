public class SCCImpl implements SCC{

    DFSImpl dfs1;
    DFSImpl dfs2;

    @Override
    public void compute(Graph g) {
        this.dfs1 = new DFSImpl();
        dfs1.search(g);
        this.dfs2 = new DFSImpl();
        dfs2.search(g.transpose(), dfs1);
    }

    @Override
    public int component(int v) {
        while(dfs2.pi[v] != -1) {
            v = dfs2.pi[v];
        }
        return v;
    }
}
