// Gerichteter gewichteter Graph.
// (Ein ungerichteter gewichteter Graph kann als gerichteter gewichteter
// Graph repräsentiert werden, bei dem jede Kante in beiden Richtungen
// mit dem gleichen Gewicht vorhanden ist.)
public interface WeightedGraph extends Graph{
    // Gewicht der Kante von v zu seinem i-ten direkten Nachfolger.
    // i muss im selben Bereich wie bei der Methode succ liegen.
    // (Dies muss nicht überprüft werden.)
    double weight (int v, int i);

    // Achtung:
    // Wenn man für einen gewichteten Graphen transpose() aufruft,
    // erhält man einen Graphen des Typs Graph ohne Gewichte.
}

/*
 * Anmerkungen zu allen Algorithmen:
 *
 * Der übergebene Graph g darf nicht null sein.
 * (Dies muss nicht überprüft werden.)
 *
 * Die Nachfolger eines Knotens u müssen immer in der Reihenfolge
 * g.succ(u, 0) bis g.succ(u, g.deg(u) - 1) durchlaufen werden.
 *
 * Auf einem Objekt einer Implementierungsklasse XYZImpl muss als
 * erstes ein Algorithmus (d. h. je nach Klasse eine der Methoden
 * search, sort, compute, bellmanFord, dijkstra) ausgeführt werden.
 * Anschließend kann die vom Algorithmus ermittelte Information
 * abgefragt werden (je nach Klasse mit den Methoden dist, pred,
 * det, fin, sequ, component; d. h. wenn eine dieser Methoden vor der
 * Ausführung eines Algorithmus aufgerufen wird, darf ihr Verhalten
 * undefiniert sein).
 * Eine mehrmalige Ausführung eines Algorithmus auf demselben Objekt
 * muss nicht unterstützt werden (d. h. man sollte für jede Ausführung
 * eines Algorithmus ein neues Objekt der Klasse XYZImpl erzeugen).
 */