import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class DFSImpl implements DFS{

    int nextDeltaPhi = 1;
    boolean isSortable = true;

    int[] pi;
    int[] delta;
    int[] phi;

    public void search(Graph g) {
        pi = new int[g.size()];
        delta = new int[g.size()];
        phi = new int[g.size()];
        for (int u = 0; u < g.size(); u++) {
            if (delta[u] == 0 && phi[u] == 0) {
                pi[u] = -1;
                searchPartGraph(g, u);
            }
        }
    }

    public void searchPartGraph(Graph g, int u) {
        delta[u] = nextDeltaPhi++;
        for (int i = 0; i < g.deg(u); i++) {
            int v = g.succ(u, i);
            if (delta[v] == 0 && phi[v] == 0) {
                pi[v] = u;
                searchPartGraph(g, v);
            }
            if (delta[v] > 0 && phi[v] == 0) {
                isSortable = false;
            }
        }
        phi[u] = nextDeltaPhi++;
    }

    @Override
    public void search(Graph g, DFS d) {
        pi = new int[g.size()];
        delta = new int[g.size()];
        phi = new int[g.size()];
        for (int i = 0; i < g.size(); i++) {
            int u = d.sequ(g.size() - (i + 1));
            if (delta[u] == 0 && phi[u] == 0) {
                //u/i ist weiß
                pi[u] = -1;
                searchPartGraph(g, u);
            }
        }
    }

    @Override
    public boolean sort(Graph g) {
        this.search(g);
        return this.isSortable;
    }

    @Override
    public int det(int v) {
        return delta[v];
    }

    @Override
    public int fin(int v) {
        return phi[v];
    }

    @Override
    public int sequ(int i) {
        int[] nodescopy = new int[phi.length];
        int[] phicopy = new int[phi.length];
        for (int j = 0; j < phi.length; j++) {
            nodescopy[j] = j;
            phicopy[j] = phi[j];
        }

        for (int j = 0; j < nodescopy.length; j++) {
            for (int k = 0; k < nodescopy.length - 1; k++) {
                if (phicopy[k] > phicopy[k + 1]) {
                    int temp = phicopy[k];
                    phicopy[k] = phicopy[k + 1];
                    phicopy[k + 1] = temp;

                    int temp2 = nodescopy[k];
                    nodescopy[k] = nodescopy[k + 1];
                    nodescopy[k + 1] = temp2;
                }
            }
        }
        return nodescopy[i];
    }
}
