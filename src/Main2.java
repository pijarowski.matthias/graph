public class Main2 {
    public static void main(String[] args) {
        int[][] matrix = new int[][] {
                new int[]{1, 2, 3},
                new int[]{2},
                new int[]{4},
                new int[]{3, 5, 6},
                new int[]{},
                new int[]{2},
                new int[]{},
        };
        double[][] weights = new double[][] {
                new double[]{4, 10, 7},
                new double[]{6},
                new double[]{2},
                new double[]{1, 6, 6},
                new double[]{},
                new double[]{4},
                new double[]{},
        };

        Graph g = new GraphImpl(matrix);

        System.out.println("BFS");
        BFS bfs = new BFSImpl();
        bfs.search(g, 0);
        for (int i = 0; i < g.size(); i++) {
            System.out.println("Dist: " + bfs.dist(i) + " Pred: " + bfs.pred(i));
        }

        System.out.println("\nDFS");
        DFS dfs = new DFSImpl();
        dfs.search(g);
        for (int i = 0; i < g.size(); i++) {
            System.out.println("Det: " + dfs.det(i) + " Fin: " + dfs.fin(i));
        }

        dfs = new DFSImpl();
        System.out.println("\nSortable: " + dfs.sort(g));

        System.out.println("\nSCC");
        SCC scc = new SCCImpl();
        scc.compute(g);
        for (int i = 0; i < g.size(); i++) {
            System.out.println(scc.component(i));
        }

        int[][] matrix_ung = new int[][] {
                new int[]{1, 2, 3},
                new int[]{0, 2},
                new int[]{0, 1, 4, 5},
                new int[]{0, 3, 5, 6},
                new int[]{2},
                new int[]{2, 3},
                new int[]{3},
        };
        double[][] weights_ung = new double[][] {
                new double[]{4, 10, 7},
                new double[]{4, 6},
                new double[]{10, 6, 2, 4},
                new double[]{7, 1, 6, 6},
                new double[]{2},
                new double[]{4, 6},
                new double[]{6},
        };

        System.out.println("\nMSF");
        WeightedGraph weightedGraph_ung = new WeightedGraphImpl(matrix_ung, weights_ung);
        MSF msf = new MSFImpl();
        msf.compute(weightedGraph_ung, 0);
        for (int i = 0; i < weightedGraph_ung.size(); i++) {
            System.out.println(msf.pred(i));
        }

        WeightedGraph weightedGraph = new WeightedGraphImpl(matrix, weights);
        System.out.println("\nSP bellman ford");
        SP sp = new SPImpl();
        System.out.println("Bellmanford, no neg cycles: " + sp.bellmanFord(weightedGraph, 0));
        for (int i = 0; i < weightedGraph.size(); i++) {
            System.out.println("Dist: " + sp.dist(i) + ", pred: " + sp.pred(i));
        }

        System.out.println("\nDijkstra");
        SP sp2 = new SPImpl();
        sp2.dijkstra(weightedGraph, 0);
        for (int i = 0; i < weightedGraph.size(); i++) {
            System.out.println("Dist: " + sp2.dist(i) + ", pred: " + sp2.pred(i));
        }
    }
}
