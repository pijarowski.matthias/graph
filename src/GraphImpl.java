import java.util.ArrayList;

public class GraphImpl implements Graph{

    private int[][] adjMatrix;

    public GraphImpl(int[][] adjMatrix) {
        this.adjMatrix = adjMatrix;
    }

    @Override
    public int size() {
        return this.adjMatrix.length;
    }

    @Override
    public int deg(int v) {
        return this.adjMatrix[v].length;
    }

    @Override
    public int succ(int v, int i) {
        return this.adjMatrix[v][i];
    }

    @Override
    public Graph transpose() {
        ArrayList<ArrayList<Integer>> g = new ArrayList<>();
        for (int i = 0; i < this.size(); i++) {
            g.add(new ArrayList<>());
        }

        for (int i = 0; i < this.size(); i++) {
            for (int nv: this.adjMatrix[i]) {
                g.get(nv).add(i);
            }
        }

        int[][] tempG = new int[g.size()][];
        for (int i = 0; i < g.size(); i++) {
            ArrayList<Integer> row = g.get(i);
            tempG[i] = new int[row.size()];
            for (int j = 0; j < row.size(); j++) {
                tempG[i][j] = row.get(j);
            }
        }

        return new GraphImpl(tempG);
    }
}
