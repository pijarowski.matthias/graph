import java.util.ArrayList;

public class MSFImpl implements MSF{
    int[] pi;

    @Override
    public void compute(WeightedGraph g, int s) {
        BinHeap<Double, Integer> prioQ = new BinHeap<>();
        ArrayList<BinHeap.Entry<Double, Integer>> entries = new ArrayList<>();
        pi = new int[g.size()];
        for (int v = 0; v < g.size(); v++) {
            entries.add(prioQ.insert(Double.POSITIVE_INFINITY, v));
            pi[v] = NIL;
        }

        prioQ.remove(entries.get(s));
        pi[s] = NIL;

        int u = s;
        while (!prioQ.isEmpty()) {
            for (int i = 0; i < g.deg(u); i++) {
                int v = g.succ(u, i);
                if (prioQ.contains(entries.get(v)) && (g.weight(u, i) < entries.get(v).prio())) {
                    prioQ.changePrio(entries.get(v), g.weight(u, i));
                    pi[v] = u;
                }
            }
            u = prioQ.extractMin().data();
        }
    }

    @Override
    public int pred(int v) {
        return this.pi[v];
    }
}
