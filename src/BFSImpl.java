public class BFSImpl implements BFS{
    private int[] delta;
    private int[] pi;
    
    @Override
    public void search(Graph g, int s) {
        //1
        delta = new int[g.size()];
        pi = new int[g.size()];
        for (int v = 0; v < g.size(); v++) {
            delta[v] = INF;
            pi[v] = NIL;
        }

        //2
        delta[s] = 0;

        //3
        int prio = 0;
        BinHeap<Integer, Integer> fifo = new BinHeap<>();
        fifo.insert(prio, s);

        //4
        while (!fifo.isEmpty()) {
            //4.1
            int u = fifo.extractMin().data();
            //4.2
            for (int i = 0; i < g.deg(u); i++) {
                int v = g.succ(u, i);
                if (delta[v] == INF) {
                    delta[v] = delta[u] + 1;
                    pi[v] = u;
                    fifo.insert(++prio, v);
                }
            }
        }
    }

    @Override
    public int dist(int v) {
        return delta[v];
    }

    @Override
    public int pred(int v) {
        return pi[v];
    }
}
