public class main {
    public static void main(String[] args) {
        int [][] matrix = new int[][] {
                new int[]{1, 3, 5},
                new int[]{},
                new int[]{3, 6},
                new int[]{4, 5},
                new int[]{0, 5},
                new int[]{1},
                new int[]{2, 4}
        };
        Graph g = new GraphImpl(matrix);

        System.out.println("BFS");
        BFS bfs = new BFSImpl();
        bfs.search(g, 0);
        for (int i = 0; i < g.size(); i++) {
            System.out.println(bfs.dist(i) + " " + bfs.pred(i));
        }

        System.out.println("\nDFS");
        DFS dfs = new DFSImpl();
        dfs.search(g);
        for (int i = 0; i < g.size(); i++) {
            System.out.println(dfs.det(i) + " " + dfs.fin(i));
        }
        System.out.println("sortable: " + dfs.sort(g));

        System.out.println("\nSCC");
        SCC scc = new SCCImpl();
        scc.compute(g);
        for (int i = 0; i < g.size(); i++) {
            System.out.println(scc.component(i));
        }

        System.out.println("\nWeighted Graph Algorithms");
        int[][] wMatrix = new int[][] {
                new int[]{1, 4, 5},
                new int[]{0, 2, 3, 4, 5},
                new int[]{1, 3, 4},
                new int[]{1, 2, 4},
                new int[]{0, 1, 2, 3, 5},
                new int[]{0, 1, 4}
        };

        double [][] weights = new double[][] {
                new double[]{3, 1, 5},
                new double[]{3, 8, 7, 2, 5},
                new double[]{8, 5, 7},
                new double[]{7, 5, 8},
                new double[]{1, 2, 7, 8, 4},
                new double[]{5, 5, 4}
        };

        System.out.println("\nMSF");
        WeightedGraph weightedGraph = new WeightedGraphImpl(wMatrix, weights);
        MSF msf = new MSFImpl();
        msf.compute(weightedGraph, 0);
        for (int i = 0; i < weightedGraph.size(); i++) {
            System.out.println(msf.pred(i));
        }

        int[][] wMatrix2 = new int[][] {
                new int[]{3},
                new int[]{0},
                new int[]{0, 1},
                new int[]{1, 2}
        };
        double[][] weights2 = new double[][] {
                new double[]{4},
                new double[]{3},
                new double[]{6, 2},
                new double[]{5, 1}
        };
        WeightedGraph weightedGraph1 = new WeightedGraphImpl(wMatrix2, weights2);

        int[][] wMatrix3 = new int[][] {
                new int[]{1, 2},
                new int[]{2},
                new int[]{1},
        };
        double[][] weights3 = new double[][] {
                new double[]{1, 1},
                new double[]{-1},
                new double[]{-1},
        };
        WeightedGraph weightedGraph2 = new WeightedGraphImpl(wMatrix3, weights3);

        System.out.println("\nSP bellman ford");
        SP sp = new SPImpl();
        System.out.println("Bellmanford, no neg cycles: " + sp.bellmanFord(weightedGraph1, 0));
        for (int i = 0; i < weightedGraph1.size(); i++) {
            System.out.println("Dist: " + sp.dist(i) + ", pred: " + sp.pred(i));
        }

        SP sp1 = new SPImpl();
        System.out.println("Bellmanford, neg cycles: " + sp1.bellmanFord(weightedGraph2, 0));

        System.out.println("\nDijkstra");
        SP sp2 = new SPImpl();
        sp2.dijkstra(weightedGraph1, 0);
        for (int i = 0; i < weightedGraph1.size(); i++) {
            System.out.println("Dist: " + sp2.dist(i) + ", pred: " + sp2.pred(i));
        }
    }
}
