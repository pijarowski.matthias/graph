import java.util.ArrayList;

public class SPImpl implements SP{

    double[] delta;
    int[] pi;

    @Override
    public boolean bellmanFord(WeightedGraph g, int s) {
        delta = new double[g.size()];
        pi = new int[g.size()];
        for (int v = 0; v < g.size(); v++) {
            delta[v] = INF;
            pi[v] = NIL;
        }
        delta[s] = 0;

        for (int i = 0; i < g.size() - 1; i++) {
            for (int u = 0; u < g.size(); u++) {
                for (int j = 0; j < g.deg(u); j++) {
                    int v = g.succ(u, j);
                    double weight = g.weight(u, j);
                    if (delta[u] + weight < delta[v]) {
                        delta[v] = delta[u] + weight;
                        pi[v] = u;
                    }
                }
            }
        }

        for (int u = 0; u < g.size(); u++) {
            for (int i = 0; i < g.deg(u); i++) {
                int v = g.succ(u, i);
                double weight = g.weight(u, i);
                if (delta[u] + weight < delta[v]) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public void dijkstra(WeightedGraph g, int s) {
        this.delta = new double[g.size()];
        this.pi = new int[g.size()];
        for (int v = 0; v < g.size(); v++) {
            delta[v] = INF;
            pi[v] = NIL;
        }
        delta[s] = 0;

        BinHeap<Double, Integer> prioQ = new BinHeap<>();
        ArrayList<BinHeap.Entry<Double, Integer>> entries = new ArrayList<>();
        for (int v = 0; v < g.size(); v++) {
            entries.add(prioQ.insert(delta[v], v));
        }

        while (!prioQ.isEmpty()) {
            int u = prioQ.extractMin().data();
            for (int i = 0; i < g.deg(u); i++) {
                int v = g.succ(u, i);
                if (prioQ.contains(entries.get(v))) {
                    double weight = g.weight(u, i);
                    if (delta[u] + weight < delta[v]) {
                        delta[v] = delta[u] + weight;
                        pi[v] = u;
                        prioQ.changePrio(entries.get(v), delta[v]);
                    }
                }
            }
        }
    }

    @Override
    public double dist(int v) {
        return this.delta[v];
    }

    @Override
    public int pred(int v) {
        return this.pi[v];
    }
}
